<?php

/**
 * The ajax functionality of the plugin.
 *
 * @link       bf-a
 * @since      1.0.0
 *
 * @package    bf-eretailer-button
 * @subpackage bf-eretailer-button/public
 */

/**
 * The ajax functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    bf-eretailer-button
 * @subpackage bf-eretailer-button/public
 * @author     BF <norberto_tongoy@b-f.com>
 */
class BF_Eretailer_Button_Ajax {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * URL api call.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    URL api call.
	 */
	private $url;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {
		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->url = 'https://epremise-api.b-fonline.com/product/';
	}

	public function api_epremise_locator() {
		$product_code = $_GET['code'];
		$response = wp_remote_get( $this->url . $product_code );
		try {
			// Note that we decode the body's response since it's the actual JSON feed
			$json = json_encode( $response['body'] );
		} catch ( Exception $ex ) {
			$json = null;
		}
		exit( $json );
	}

}


?>