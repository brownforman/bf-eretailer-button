<?php

/**
 * Fired during plugin activation
 *
 * @link       bf-a
 * @since      1.0.0
 *
 * @package    bf-eretailer-button
 * @subpackage bf-eretailer-button/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    bf-eretailer-button
 * @subpackage bf-eretailer-button/includes
 * @author     BF <norberto_tongoy@b-f.com>
 */
class BF_Eretailer_Button_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
