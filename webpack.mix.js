const mix = require('laravel-mix');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// npm run dev --locale=en_US
// npm run dev --locale=en_AU

mix.webpackConfig({
   module: {
      rules: [
         {
            test: /\.s(c|a)ss$/,
            loaders: [
               {
                  loader: "sass-loader",
               }
            ]
         }
      ]
   }
});

mix.js('resources/js/app.js', 'public/js/bf-epremise-button.js');
if ( process.env.npm_config_locale ) {
   var locale = process.env.npm_config_locale;
   mix.sass('resources/sass/' + locale + '/app.scss', 'public/css/bf-epremise-button-' + locale + '.css');
} else {
   mix.sass('resources/sass/app.scss', 'public/css/bf-epremise-button.css');
}