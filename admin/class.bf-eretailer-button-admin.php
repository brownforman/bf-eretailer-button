<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       bf-a
 * @since      1.0.0
 *
 * @package    bf-eretailer-button
 * @subpackage bf-eretailer-button/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    bf-eretailer-button
 * @subpackage bf-eretailer-button/admin
 * @author     BF <norberto_tongoy@b-f.com>
 */
class BF_Eretailer_Button_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		add_action( 'admin_menu', array($this, 'bfeb_add_admin_menu' ) );
		add_action( 'admin_init', array($this, 'bfeb_settings_init' ) );
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in BF_Eretailer_Button_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The BF_Eretailer_Button_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/bf-eretailer-button-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in BF_Eretailer_Button_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The BF_Eretailer_Button_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/bf-eretailer-button-admin.js', array( 'vue' ), $this->version, false );

	}

	public function bfeb_add_admin_menu(  ) {
		add_options_page( 'BF E-Retailer Button', 'BF E-Retailer Button', 'manage_options', 'bf_e-retailer_button', array($this, 'bfeb_options_page' ) );
	}


	public function bfeb_settings_init(  ) {

		register_setting( 'pluginPage', 'bfeb_settings' );

		add_settings_section(
			'bfeb_pluginPage_section',
			__( 'E-Retailer Configuration', 'bf-eretailer-button' ),
			array($this, 'bfeb_settings_section_callback'),
			'pluginPage'
		);

		// add_settings_field(
		// 	'bfeb_select_field_0',
		// 	__( 'Settings field description', 'bf-eretailer-button' ),
		// 	array($this, 'bfeb_select_field_0_render'),
		// 	'pluginPage',
		// 	'bfeb_pluginPage_section'
		// );

		add_settings_field(
			'bfeb_intro_text',
			__( 'Intro Text', 'bf-eretailer-button' ),
			array($this, 'bfeb_intro_text_render'),
			'pluginPage',
			'bfeb_pluginPage_section'
		);

		add_settings_field(
			'bfeb_legal_copy',
			__( 'Legal Copy', 'bf-eretailer-button' ),
			array($this, 'bfeb_legal_copy_render'),
			'pluginPage',
			'bfeb_pluginPage_section'
		);

		add_settings_field(
			'bfeb_no_results_text',
			__( 'No Results Message', 'bf-eretailer-button' ),
			array($this, 'bfeb_no_results_text_render'),
			'pluginPage',
			'bfeb_pluginPage_section'
		);

		add_settings_field(
			'bfeb_select_plugin_country',
			__( 'Select Plugin Country', 'bf-eretailer-button' ),
			array($this, 'bfeb_select_plugin_country_render'),
			'pluginPage',
			'bfeb_pluginPage_section'
		);


	}

	public function bfeb_select_plugin_country_render(  ) {
		$options = get_option( 'bfeb_settings' );
		?>
		<select name='bfeb_settings[bfeb_select_plugin_country]'>
			<option value='en' <?php selected( $options['bfeb_select_plugin_country'], 'en' ); ?> >Default</option>
			<option value='en_US' <?php selected( $options['bfeb_select_plugin_country'], 'en_US' ); ?> >USA</option>
			<option value='en_AU' <?php selected( $options['bfeb_select_plugin_country'], 'en_AU' ); ?> >Australia</option>
			<option value='en_GB' <?php selected( $options['bfeb_select_plugin_country'], 'en_GB' ); ?> >UK</option>
			<option value='de_DE' <?php selected( $options['bfeb_select_plugin_country'], 'de_DE' ); ?> >Germany</option>
			<option value='fr_FR' <?php selected( $options['bfeb_select_plugin_country'], 'fr_FR' ); ?> >France</option>
		</select>

	<?php

	}


	public function bfeb_select_field_0_render(  ) {
		$options = get_option( 'bfeb_settings' );
		?>
		<select name='bfeb_settings[bfeb_select_field_0]'>
			<option value='1' <?php selected( $options['bfeb_select_field_0'], 1 ); ?>>Option 1</option>
			<option value='2' <?php selected( $options['bfeb_select_field_0'], 2 ); ?>>Option 2</option>
		</select>

	<?php

	}


	public function bfeb_intro_text_render(  ) {
		$options = get_option( 'bfeb_settings');
		?>
		<textarea cols='40' rows='5' name='bfeb_settings[bfeb_intro_text]'><?php echo $options['bfeb_intro_text']; ?></textarea>
		<?php

	}


	public function bfeb_legal_copy_render(  ) {
		$options = get_option( 'bfeb_settings');
		$domain = strtoupper($_SERVER['HTTP_HOST']);
		$default_text = "By choosing one of links above, you are leaving $domain and going to a different website. All content on the retailer web site is the responsibility of it's owner including, without limitation, protection of your privacy which is governed by the policy of that web site. We encourage you to review the terms of use, privacy and cookie policies of that website.";
		?>
		<textarea cols='40' rows='5' name='bfeb_settings[bfeb_legal_copy]'><?php echo $options['bfeb_legal_copy'] ?: $default_text; ?></textarea>
		<?php

	}


	public function bfeb_no_results_text_render(  ) {
		$options = get_option( 'bfeb_settings' );
		?>
		<textarea cols='40' rows='5' name='bfeb_settings[bfeb_no_results_text]'><?php echo $options['bfeb_no_results_text'] ?: "No results for your location."; ?></textarea>
		<?php

	}


	public function bfeb_settings_section_callback(  ) {
		echo __( '', 'bf-eretailer-button' );
	}


	public function bfeb_options_page(  ) { ?>
			<form action='options.php' method='post'>

				<h2>BF E-Retailer Button</h2>

				<p>Lookup Market Product Codes: <a href="https://epremise-api.b-fonline.com/tools/market_product_codes" target="_blank">https://epremise-api.b-fonline.com/tools/market_product_codes</a></p>

				<?php
				settings_fields( 'pluginPage' );
				do_settings_sections( 'pluginPage' );
				submit_button();
				?>

			</form>
			<?php

	}


}
