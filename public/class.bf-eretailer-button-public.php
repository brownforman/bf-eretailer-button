<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       bf-a
 * @since      1.0.0
 *
 * @package    bf-eretailer-button
 * @subpackage bf-eretailer-button/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    bf-eretailer-button
 * @subpackage bf-eretailer-button/public
 * @author     BF <norberto_tongoy@b-f.com>
 */
class BF_Eretailer_Button_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		$this->options = get_option( 'bfeb_settings', '');
		$locale = isset( $this->options['bfeb_select_plugin_country'] ) ? ( '-' . $this->options['bfeb_select_plugin_country'] ) : '';
		$cssFilePath = plugin_dir_path( __FILE__ ) . 'css/bf-epremise-button'.$locale.'.css';

		if( file_exists($cssFilePath) ) {
			$this->cssFileURI = plugin_dir_url( __FILE__ ) . 'css/' . basename($cssFilePath);
		} else {
			$this->cssFileURI = plugin_dir_url( __FILE__ ) . 'css/bf-epremise-button.css';
		}

		$jsFilePath = glob( plugin_dir_path( __FILE__ ) . 'js/bf-epremise-button.js' );
		$this->jsFileURI = plugin_dir_url( __FILE__ ) . 'js/' . basename($jsFilePath[0]);

	}

	public function register_shortcodes() {
		add_shortcode( 'bf-eretailer-button', array( $this, 'bf_eretailer_button_shortcode') );
		add_shortcode( 'bf-eretailer-brand', array( $this, 'bf_eretailer_brand_shortcode') );
	}


	public function bf_eretailer_button_shortcode($atts = array()){
		//$this->register_scripts();
		$this->enqueue_scripts();
		$this->enqueue_styles();

    $market_product_code = !empty($atts['market_product_code']) ? $atts['market_product_code'] : null;
		$button_id = !empty($atts['button_id']) ? $atts['button_id'] : null;
		$button_label = !empty($atts['button_label']) ? $atts['button_label'] : 'Buy Now';
		$button_image = !empty($atts['button_image']) ? $atts['button_image'] : null;

		$custom_class = !empty($atts['custom_class']) ? $atts['custom_class'] : null;


		$banner_image = '';
		if ( isset($atts['banner_image']) ) {
			$banner_image = trim($atts['banner_image']);
		}
		$fullscreen = false;
		if ( isset($atts['fullscreen']) ) {
			$fullscreen = $atts['fullscreen'];
		}

		$domain = strtoupper($_SERVER['HTTP_HOST']);

		// $options = get_option( 'bfeb_settings', '');

		$legalcopy = "By choosing one of links above, you are leaving $domain and going to a different website. All content on the retailer web site is the responsibility of it's owner including, without limitation, protection of your privacy which is governed by the policy of that web site. We encourage you to review the terms of use, privacy and cookie policies of that website.";
		$legalcopy = !empty($this->options['bfeb_legal_copy']) ? $this->options['bfeb_legal_copy'] : $legalcopy;

		$no_results = "No results for your location.";
		$no_results = !empty($this->options['bfeb_no_results_text']) ? $this->options['bfeb_no_results_text'] : $no_results;

		$intro_text = !empty($this->options['bfeb_intro_text']) ? $this->options['bfeb_intro_text'] : "";
		preg_match_all('/\[(.*?)\]/', $intro_text, $match);

		array_walk($match[1], function($val) use($atts, &$intro_text) {
			$txt = !empty($atts[$val]) ? $atts[$val] : '';
			$intro_text = str_replace('['.$val.']', $txt, $intro_text);
		});

		$vue_atts = esc_attr(json_encode([
			'market_product_code' => $market_product_code,
			'button_label' => $button_label,
			'button_image' => $button_image,
			'button_id' => $button_id,
			'custom_class' => $custom_class,
			'banner_image' => $banner_image,
			'fullscreen' => $fullscreen,
			'legalcopy' => $legalcopy,
			'noresults' => $no_results,
			'introtext' => $intro_text
		]));

		return "<div id='eretailer-wrapper' class='eretailer-wrapper-app' data-atts='{$vue_atts}'><div>loading eRetailer data...</div></div>";
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in BF_Eretailer_Button_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The BF_Eretailer_Button_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_style( $this->plugin_name, $this->cssFileURI, array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		wp_enqueue_script( $this->plugin_name, $this->jsFileURI, array(), $this->version, 'all' );
	}

}


?>
