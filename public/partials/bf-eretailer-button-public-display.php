<?php

/**
 * Default Form
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       bf-a
 * @since      1.0.0
 *
 * @package    bf-eretailer-button
 * @subpackage bf-eretailer-button/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->