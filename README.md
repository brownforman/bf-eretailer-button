**Use**
```
[bf-eretailer-button market_product_code="Australia_000000000002732035" button_label="Buy Now" fullscreen="false" banner_image="https://ecommerce-static.brown-forman.com/Banner_images/0000_jdtw.jpg"]
```

### Attributes:

+ market_product_code : String (Salesforce Product Code)
+ button_label : String (free text)
+ fullscreen : Boolean (true | false, optional)
+ banner_image : String - (URL, optional)
+ button_image : String - (URL, optional) - Replace standard button with a clickable image
+ button_id : String - Add ID to button (no ID by default)
+ custom_class : String - Add custom class to button (only bf-eretail-locator-button by default)

### Admin Panel:
#### Dashboard -> Settings -> BF E-Retailer Button

- Intro Text: Text to appear at top of Dialog, below optional banner_image. Supports HTML. Dynamic values can be added using brackets. Text in brackets will be replaced with value from corresponding shortcode attribute.
  - Example: Find Some [friendly_name].
  - [friendly_name] would be replaced with the friendly_name attribute from the shortcode.
- Legal Copy: Legal Copy to appear at bottom of Dialog. Supports HTML, does not support dynamic values. Default value will autofill.
- No Results Message: Message to display when no results are available. Supports HTML, does not support dynamic values. Default value will autofill.
- Select Plugin Country: Custom styling per country/market. Default value for global use.

### Find Market Product Codes:
https://epremise-api.b-fonline.com/tools/market_product_codes


### For Developers

The plugin should be updated to match the look and feel of the website it is on. This is accomplished by making edits to the files in resources/sass/ and the files in resources/js.

To compile edits, follow these steps:
1. Run `npm install`
2. Run `npm run watch` to monitor the resources directory for changes and compile automatically.
3. Run `npm run prod` to compile changes for production.

`npm run dev` can be used to compile a non-minified version of the code.

**Country paramenter build**

Make a copy of the `sass/LocaleCodeTemplate` with the exact name from the [Wordpress Locale Code](https://wpcentral.io/internationalization/). Make all stylesheet customizations under the newly created folder (e.g. en_US). Build the stylesheet using the commands below.

`npm run dev --locale=en_US`

`npm run production --locale=en_US`


The build system uses Laravel Mix, Vue & Webpack. To keep build system updated, periodically check changes here and apply any that are necessary to keep the build system working: https://github.com/laravel/laravel/blob/master/package.json and https://laravel.com/docs/5.8/frontend#using-react.


**Styling button and dialog box**

> bf-eretailer-button/resources/assets/js

+ Button - App.vue
+ Dialog - components/Dialog.vue
+ Fullscreen - components/Fullscreen.vue


**Reference**

+ [Vue Reference](https://bootstrap-vue.js.org/docs/reference)


# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.0] - 2020‑07‑16
### Changed
- Remove hard coded reference to US states.
- Update vue components to use [Subgeo API](https://sites.google.com/bfexternal.com/bf-im-api-docs/epremise-data/get-subgeographies).
- Update const PLUGIN_NAME_VERSION.

## [1.0.0] - 2019‑10‑04
### Added
- Initial commit.
- GTM event tracking.
